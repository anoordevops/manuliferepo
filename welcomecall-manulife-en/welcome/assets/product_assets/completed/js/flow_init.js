/*
 Animation Flow
 PageByPage
 */
window.stage = {
    "screens": []
};


window.common_screens = [

    
    {
        "sprite_animations": [

        

           {
                "sprite": "canvas_transparent",
                "x": 530,
                "y": 300,
                "loop": false,
                "timing": 0,
                "delay": 1,
                "toTopObj": 1,
                "anchor": [0.5, 0],
                 "scale":0.8,
                "onClickFn": "goToPage(1)"
            },            

       ],

       "text_animations": [

        {
            "text": [{
                "content": "Your Link is already completed"
            }],
            "sx": 550,
            "sy": 1150,
            "x": 550,
            "y": 1150,
            "size": 55,
            "weight": "normal",
            "color": "#ec1c24",
            "tween_type": "Elastic.easeOut",
            "timing": 200,
            "delay": 0,
            "anchor": [0.5, 0],
            "strokeThickness":1,
           
           "wordWrap":true,
           "wordWrapWidth":800,
        },

        ],
        "functions": [
           
            {
                "fn": "SetBGTile('complete_wel')",  //Reliance_Welcome //Reliance_BG
                "delay": 0
            },

  
            ],

        "name": "Language Selection",
        "timing": -1,
        "index": 0
    },

{
    "sprite_animations": [
     
      
    {"sprite": "save","x": 550,"y": 1790,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":0.8,"onClickFn":"goToPage_otp(4,'*Incorrect OTP',540,1300)"},
     
    {"sprite": "bar_08","x": 540,"y": 1690,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":0.9,"onClickFn":"resend_otp()"},  
     
    
          
         ],

    "text_animations": [

      {"text": [{"content":"Verification"}], "sx": 540,"sy": 200,"x": 540 ,"y": 200,"size": 50,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0.5,0],"align":"center" },

      {"text": [{"content":"Proceed"}], "sx": 540,"sy": 1800,"x": 540 ,"y": 1800,"size": 45,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0.5,0],"align":"center","strokeThickness":1 },
      {"text": [{"content":"Please enter the OTP sent to your registered mobile number"}], "sx": 540,"sy": 700,"x": 540 ,"y": 700,"size": 48,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0.5,0],"align":"center","wordWrap":true,"wordWrapWidth":900 },
  
      {"text": [{"content":"Haven't received a message?"}], "sx": 540,"sy": 1600,"x": 540 ,"y": 1600,"size": 45,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0.5,0],"align":"center","wordWrap":true,"wordWrapWidth":1000 },
  
      {"text": [{"content":"Resend OTP"}], "sx": 540,"sy": 1680,"x": 540 ,"y": 1680,"size": 45,"weight":"bold","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0.5,0],"align":"center","wordWrap":true,"wordWrapWidth":1000,"strokeThickness":1 },
  
  
        ],

       
    "sound_list": [
        {
            "sound": ['newotp']
            }
        ],
    "functions": [
  
   { "fn": "SetBGTile('manu_common')",   "delay": 0},
    {"fn": "sounnnn()", "delay": 0 },   
   { "fn": "showOTP()",   "delay": 0,}, 
   { "fn": "otp_generate()",   "delay": 0},   
          
 
 
        ],
    "name": "Manulife_3",
    "timing": -1,
    "index": 1
  },
    
//     "sprite_animations": [

       
//          {
//                 "sprite": "English",
//                 "x": 540,
//                 "y": 800,
//                 "loop": false,
//                 "timing": 0,
//                 "delay": 0.5,
//                 "toTopObj": 1,
//                 "anchor": [0.5, 0],
//                  "scale":1,
//                 "onClickFn": "loadLangFlow(\"eng\",\"normal\",2)"
//             },

//             {
//                 "sprite": "ind",
//                 "x": 540,
//                 "y": 1000,
//                 "loop": false,
//                 "timing": 0,
//                 "delay": 0.5,
//                 "toTopObj": 1,
//                 "anchor": [0.5, 0],
//                  "scale":1,
//             },


//         ],
//     "text_animations": [

//         {
//             "text": [{
//                 "content": "Please choose your preferred language"
//             }],
//             "sx": 550,
//             "sy": 550,
//             "x": 550,
//             "y": 550,
//             "size": 55,
//             "weight": "normal",
//             "color": "#000000",
//             "tween_type": "Elastic.easeOut",
//             "timing": 200,
//             "delay": 0,
//             "anchor": [0.5, 0],
           
//            "wordWrap":true,
//            "wordWrapWidth":800,
//         },

//          {
//             "text": [{
//                 "content": "Filipino"
//             }],
//             "sx": 550,
//             "sy": 1030,
//             "x": 550,
//             "y": 1030,
//             "size": 48,
//             "weight": 0,
//             "color": "#FFFFFF",
//             "tween_type": "Elastic.easeOut",
//             "timing": 200,
//             "delay": 1,
//             "anchor": [0.5, 0]
//         },

//         ],
//     "sound_list": [
//         {
//             "sound": ['choose_lang']
//             }
//         ],
//     "functions": [

//         {
//             "fn": "SetBGTile('manu_common')",
//             "delay": 0
//         },

//         {"fn": "sounnnn()", "delay": 0 },
  
//         { "fn": "init()",  "delay": 0 },
//        // { "fn": "validate_otp()",  "delay": 0 }

           
//         ],
//     "name": "Start",
//     "timing": -1,
//     "index": 1
// }


];



for (var i = 0; i < window.common_screens.length; i++) {
    window.stage.screens.push(window.common_screens[i]);
}
