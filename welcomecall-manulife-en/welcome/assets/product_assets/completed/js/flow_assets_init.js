/*
 Custom Assets
 */

// Define Paths
var IMG_COM_PATH = 'assets/images/common/';
var CUSTOM_IMG_PATH = './assets/images/custom/';
var PRODUCT_PATH = './assets/product_assets/'+window.flow_slug+'/';
var PRODUCT_IMG_PATH = PRODUCT_PATH+'images/';
var COMMON_PRODUCT_IMG_PATH = './assets/images/common/product/';
var COMMON_PRODUCT_LANG_IMG_PATH = COMMON_PRODUCT_IMG_PATH+'eng/';
var PRODUCT_SCENE_AUDIO_PATH = PRODUCT_PATH+'audio/eng/scenes/';

var COMMON_JS_PATH = './assets/js/common/';

var TYPE_SCENE_AUDIO_PATH = './assets/audio/product/eng/scenes/';
var TYPE_COMMON_AUDIO_PATH = './assets/audio/product/eng/common/';



var COMMON_IMG_PATH = './assets/images/common/product/';
var COMMON_LANG_IMG_PATH = COMMON_IMG_PATH+'eng/';

function customAssets()
{
    // SpriteSheet

 

    // Scripts

    // Camera Plugin

game.load.image('English', IMG_COM_PATH+'English.png');
game.load.image('ind', IMG_COM_PATH+'ind.png');

//game.load.image('manu_wel', IMG_COM_PATH+'manu_wel.png');

//game.load.spritesheet('10', 'assets/10.png', { frameWidth: 210, frameHeight: 240 });
game.load.spritesheet('manu_wel', IMG_COM_PATH + 'manu_wel.png', { frameWidth: 1080, frameHeight: 1920 });

game.load.spritesheet('manu_otp', IMG_COM_PATH + 'manu_otp.png',  { frameWidth: 1080, frameHeight: 1920 });
game.load.spritesheet('manu_common', IMG_COM_PATH + 'manu_common.png',  { frameWidth: 1080, frameHeight: 1920 });
game.load.spritesheet('manu_agree', IMG_COM_PATH + 'manu_agree.png',  { frameWidth: 1080, frameHeight: 1920 });
game.load.spritesheet('tq_manu', IMG_COM_PATH + 'tq_manu.png', { frameWidth: 1080, frameHeight: 1920 });

game.load.spritesheet('canvas_transparent', IMG_COM_PATH + 'canvas_transparent.png',  { frameWidth: 1080, frameHeight: 1920 });

game.load.spritesheet('manu_load', IMG_COM_PATH + 'manu_load.png',  { frameWidth: 1080, frameHeight: 1920 });

game.load.spritesheet('complete_wel', IMG_COM_PATH + 'complete_wel.png',  { frameWidth: 1080, frameHeight: 1920 });

 //game.load.audio('backmusic', TYPE_SCENE_AUDIO_PATH+'backmusic.mp3');

//game.load.audio('choose_lang', TYPE_SCENE_AUDIO_PATH+'choose_lang.mp3');

  // Audio
 //game.load.script('webcam', COMMON_JS_PATH+'camera/Webcam.js');
 
 
 game.load.image('right-arrow', COMMON_LANG_IMG_PATH+'right-arrow.png');
game.load.image('left-arrow', COMMON_LANG_IMG_PATH+'left-arrow.png');
game.load.image('sav', COMMON_LANG_IMG_PATH+'sav.png');

game.load.image('right-green', COMMON_LANG_IMG_PATH+'right-arrow-green.png');
game.load.image('left-green', COMMON_LANG_IMG_PATH+'left-arrow-green.png');

game.load.image('manu_ex', COMMON_LANG_IMG_PATH+'manu_ex.png');
game.load.image('manu_min', COMMON_LANG_IMG_PATH+'manu_min.png');
game.load.image('manu_black', COMMON_LANG_IMG_PATH+'manu_black.png');

game.load.image('pencil', COMMON_LANG_IMG_PATH+'pencil.png');
game.load.image('red_blus', COMMON_LANG_IMG_PATH+'red_blus.png');
game.load.image('red_sub', COMMON_LANG_IMG_PATH+'red_sub.png');
game.load.image('hashcolor', COMMON_LANG_IMG_PATH+'hashcolor.png');

game.load.image('hastick', COMMON_LANG_IMG_PATH+'hastick.png');


game.load.image('white', COMMON_LANG_IMG_PATH+'white2.png');
game.load.image('bar_08', COMMON_LANG_IMG_PATH+'bar_08.png');
game.load.image('bar_06', COMMON_LANG_IMG_PATH+'bar_06.png');

game.load.image('box_blank2', COMMON_LANG_IMG_PATH+'box_blank2.png');
game.load.image('box_blank', COMMON_LANG_IMG_PATH+'box_blank.png');


game.load.image('save1', COMMON_LANG_IMG_PATH+'save.png');


game.load.image('bg_bar', COMMON_LANG_IMG_PATH+'bg_bar.png');
game.load.image('bar_08', COMMON_LANG_IMG_PATH+'bar_08.png');
game.load.image('save', COMMON_LANG_IMG_PATH+'save - Copy.png');
game.load.image('blank', COMMON_LANG_IMG_PATH+'blank.png');


game.load.image('fb_blue', COMMON_LANG_IMG_PATH+'fb1.png');
game.load.image('fb_blue2', COMMON_LANG_IMG_PATH+'fb2.png');

game.load.image('fb_yes', COMMON_LANG_IMG_PATH+'fb_yes.png');
game.load.image('fb_no', COMMON_LANG_IMG_PATH+'fb_no.png');


game.load.image('face_detect_success', COMMON_LANG_IMG_PATH+'1_1 Face Detected Success.png');


game.load.audio('newotp', TYPE_SCENE_AUDIO_PATH+'newotp.mp3');
game.load.audio('customer_details', TYPE_SCENE_AUDIO_PATH+'customer_details.mp3');
game.load.audio('payment_details', TYPE_SCENE_AUDIO_PATH+'payment_details.mp3');
game.load.audio('autopay', TYPE_SCENE_AUDIO_PATH+'autopay.mp3');
game.load.audio('new_agree', TYPE_SCENE_AUDIO_PATH+'new_agree.mp3');

game.load.audio('policy_details', TYPE_SCENE_AUDIO_PATH+'policy_details.mp3');
game.load.audio('list_benefi', TYPE_SCENE_AUDIO_PATH+'list_benefi.mp3');

game.load.audio('purchase_experience', TYPE_SCENE_AUDIO_PATH+'purchase_experience.mp3');
game.load.audio('non_face', TYPE_SCENE_AUDIO_PATH+'non_face.mp3');
game.load.audio('datetime', TYPE_SCENE_AUDIO_PATH+'datetime.mp3');
game.load.audio('new_thankyou', TYPE_SCENE_AUDIO_PATH+'thankyou.mp3');
game.load.audio('new_download', TYPE_SCENE_AUDIO_PATH+'new_download.mp3');






 game.load.audio('backmusic', TYPE_SCENE_AUDIO_PATH+'backmusic.mp3');
// game.load.audio('manu_2', TYPE_SCENE_AUDIO_PATH+'manu_2.mp3');
// game.load.audio('manu_3', TYPE_SCENE_AUDIO_PATH+'manu_3.mp3');
// game.load.audio('manu_4', TYPE_SCENE_AUDIO_PATH+'manu_4.mp3');
// game.load.audio('manu_5', TYPE_SCENE_AUDIO_PATH+'manu_5.mp3');
// game.load.audio('manu_6', TYPE_SCENE_AUDIO_PATH+'manu_6.mp3');
// game.load.audio('manu_7', TYPE_SCENE_AUDIO_PATH+'manu_7.mp3');

// game.load.audio('nonface', TYPE_SCENE_AUDIO_PATH+'non-face-to-face.mp3');
// game.load.audio('otp', TYPE_SCENE_AUDIO_PATH+'otp.mp3');

// game.load.audio('thankyou', TYPE_SCENE_AUDIO_PATH+'thankyou.mp3');
// game.load.audio('acknow', TYPE_SCENE_AUDIO_PATH+'acknow.mp3');

// game.load.audio('benefits', TYPE_SCENE_AUDIO_PATH+'benefits.mp3');
// game.load.audio('download', TYPE_SCENE_AUDIO_PATH+'download.mp3');


  

}
