



window.stage.screens = [];

for(var i=0; i<window.common_screens.length; i++ )
{
    window.stage.screens.push(window.common_screens[i]);
}

//mass(window.p_benefi);

 var php=" PHP"; trail(window.p_PREMIUM_AMOUNT);

var screens_eng = [
    {
       
        "functions": [
            {"fn": "SetBGTile('manu_load')", "delay": 0 },
            {"fn": "langAssetsRest()", "delay": 0 },

           
           // {"fn": "specLoad()", "delay": 0 },
           
              
        ],
        "name": "Assets Loading",
        "timing": -1,
        "index": 2
    },
   
  

 {
    "sprite_animations": [
     
      {"sprite": "manu_ex","x": 540,"y": 450,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1,"onClickFn":"open_input1()"},
      {"sprite": "manu_ex","x": 540,"y": 700,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1,"onClickFn":"open_input1()"},
      {"sprite": "manu_ex","x": 540,"y": 950,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1,"onClickFn":"open_input1()"},
      {"sprite": "manu_ex","x": 540,"y": 1200,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1,"onClickFn":"open_input1()"},

       {"sprite": "pencil","x": 890,"y": 470,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1.8,"onClickFn":"open_input1()"},
      {"sprite": "pencil","x": 890,"y": 720,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1.8,"onClickFn":"open_input2()"},
      {"sprite": "pencil","x": 890,"y": 970,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1.8,"onClickFn":"open_input3()"},
      {"sprite": "pencil","x": 890,"y": 1220,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1.8,"onClickFn":"open_input4()"},

     {"sprite": "left-arrow","x": 100,"y": 1800,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1,"onClickFn":"goToPage(3)"},
      {"sprite": "right-arrow","x": 980,"y": 1800,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1,"onClickFn":"updatecustomer()"},

   //   {"sprite": "right-arrow","x": 980,"y": 1800,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1,"onClickFn":"goToPage(4)"},
 

        ],

    "text_animations": [

      {"text": [{"content":"Contact Information"}], "sx": 540,"sy": 200,"x": 540 ,"y": 200,"size": 50,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0.5,0],"align":"center","wordWrap":true,"wordWrapWidth":820 },
     
      {"text": [{"content":"Name:"}], "sx": 140,"sy": 500,"x": 140 ,"y": 500,"size": 46,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0,0],"wordWrap":true,"wordWrapWidth":820 },
      {"text": [{"content":"Address:"}], "sx": 140,"sy": 750,"x": 140 ,"y": 750,"size": 46,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0,0],"wordWrap":true,"wordWrapWidth":820 },
      {"text": [{"content":"Phone:"}], "sx": 140,"sy": 1000,"x": 140 ,"y": 1000,"size": 46,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0,0],"wordWrap":true,"wordWrapWidth":820 },
      {"text": [{"content":"Email:"}], "sx": 140,"sy": 1250,"x": 140 ,"y": 1250,"size": 46,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0,0],"wordWrap":true,"wordWrapWidth":820 },

      {"text": [{"content":"For any corrections, tap the pencil button in the appropriate field"}], "sx": 540,"sy": 1600,"x": 540 ,"y": 1600,"size": 44,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0.5,0],"align":"center","wordWrap":true,"wordWrapWidth":820 },
      
      
        ],

        "input_animations": [
        {"text": [{"content":window.p_CUSTOMER_NAME}],"placeHolder": " ","key": "in_name","sx": 410,"sy": 1730,  "x": 137,"y": 565,"size": 46,"weight": "normal","width": 730,"backgroundColor": "#c2ffae","fill": "#000000","tween_type": "Elastic.easeOut","timing": 200,"delay": 0,"anchor": [0, 0],"padding":10},
        {"text": [{"content":window.p_address}],"placeHolder": " ","key": "in_address","sx": 410,"sy": 1730,  "x": 137,"y": 805,"size": 46,"weight": "normal","width": 730,"backgroundColor": "#c2ffae","fill": "#000000","tween_type": "Elastic.easeOut","timing": 200,"delay": 0,"anchor": [0, 0],"padding":10,"type":"textarea"},
        {"text": [{"content":window.p_MOBILE_NUMBER}],"placeHolder": " ","key": "in_phone","sx": 410,"sy": 1730,  "x": 137,"y": 1050,"size": 46,"weight": "normal","width": 730,"backgroundColor": "#c2ffae","fill": "#000000","tween_type": "Elastic.easeOut","timing": 200,"delay": 0,"anchor": [0, 0],"padding":10,"type":"number"},
        {"text": [{"content":window.p_EMAIL}],"placeHolder": " ","key": "in_email","sx": 410,"sy": 1730,  "x": 137,"y": 1305,"size": 46,"weight": "normal","width": 730,"backgroundColor": "#c2ffae ","fill": "#000000","tween_type": "Elastic.easeOut","timing": 200,"delay": 0,"anchor": [0, 0],"padding":10},


        //  {"text": [{"content": email}],"placeHolder": " ","key": "in_email","sx": 400,"sy": 1380,  "x": 450,"y": 1240,"size": 40,"weight": "bold","width": 450,"backgroundColor": "#d1e8f0","fill": "#013781","tween_type": "Elastic.easeOut","timing": 200,"delay": 0,"anchor": [0, 0]},
        ],    "sound_list": [
        {
            "sound": ['customer_details']
            }
        ],
    "functions": [
  
   { "fn": "SetBGTile('manu_common')",   "delay": 0},
   { "fn": "weltext()",   "delay": 0},
    { "fn": "cursorstop()",   "delay": 0},
  //  {"fn": "background_music()", "delay": 0},
 
  
        ],
    "name": "Manulife_3",
    "timing": -1,
    "index": 3
  },
 {
    "sprite_animations": [
     
      {"sprite": "manu_ex","x": 540,"y": 600,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1},
      {"sprite": "manu_ex","x": 540,"y": 850,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1},
      {"sprite": "manu_ex","x": 540,"y": 1100,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1},
      {"sprite": "manu_ex","x": 540,"y": 1350,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1},

      {"sprite": "red_blus","x": 890,"y": 1420,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1.5,"onClickFn":"goToPage(5)"},

     {"sprite": "left-arrow","x": 100,"y": 1800,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1,"onClickFn":"customerdetails_back()"},
      {"sprite": "right-arrow","x": 980,"y": 1800,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1,"onClickFn":"goToPage(6)"},

 
        ],

    "text_animations": [

        {"text": [{"content":"Policy Details"}], "sx": 540,"sy": 200,"x": 540 ,"y": 200,"size": 50,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0.5,0],"align":"center","wordWrap":true,"wordWrapWidth":820 },
         {"text": [{"content":window.p_POLICY_NAME}], "sx": 540,"sy": 400,"x": 540 ,"y": 400,"size": 70,"weight":"bold","lineSpacing":-3,"color":"#000000","tween_type": "Elastic.easeOut","timing": 200,"delay":0,"align":"justify", "anchor":[0.5,0],"wordWrap":true,"wordWrapWidth":820,"strokeThickness":2},

        {"text": [{"content":window.policy_no}], "sx": 140,"sy": 730,"x": 140 ,"y": 730,"size": 46,"weight":"bold","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0,0],"strokeThickness":2},
        {"text": [{"content":window.p_insurance_cov+php}], "sx": 140,"sy": 975,"x": 140 ,"y": 975,"size": 46,"weight":"bold","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0,0],"strokeThickness":2},
        {"text": [{"content":window.p_initial_inves+php}], "sx": 140,"sy": 1230,"x": 140 ,"y": 1230,"size": 46,"weight":"bold","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0,0],"strokeThickness":2},
       {"text": [{"content":window.p_benefi[0]}], "sx": 140,"sy": 1470,"x": 140 ,"y": 1470,"size": 46,"weight":"bold","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0,0],"strokeThickness":2},

      {"text": [{"content":"Policy Number:"}], "sx": 140,"sy": 650,"x": 140 ,"y": 650,"size": 46,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0,0],"wordWrap":true,"wordWrapWidth":820 },
      {"text": [{"content":"Insurance Coverage:"}], "sx": 140,"sy": 900,"x": 140 ,"y": 900,"size": 46,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0,0],"wordWrap":true,"wordWrapWidth":820 },
      {"text": [{"content":"Initial Investment:"}], "sx": 140,"sy": 1150,"x": 140 ,"y": 1150,"size": 46,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0,0],"wordWrap":true,"wordWrapWidth":820 },
      {"text": [{"content":"Beneficiary/ies:"}], "sx": 140,"sy": 1400,"x": 140 ,"y": 1400,"size": 46,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0,0],"wordWrap":true,"wordWrapWidth":820 },

        ],

        "input_animations": [
      //   {"text": [{"content":""}],"placeHolder": " ","key": "in_otp","sx": 410,"sy": 1730,  "x": 420,"y": 1730,"size": 45,"weight": "bold","width": 240,"backgroundColor": "#7a7474","fill": "#000000","tween_type": "Elastic.easeOut","timing": 200,"delay": 0,"anchor": [0, 0],"height":70},
        //  {"text": [{"content": email}],"placeHolder": " ","key": "in_email","sx": 400,"sy": 1380,  "x": 450,"y": 1240,"size": 40,"weight": "bold","width": 450,"backgroundColor": "#d1e8f0","fill": "#013781","tween_type": "Elastic.easeOut","timing": 200,"delay": 0,"anchor": [0, 0]},
        ],
    "sound_list": [
        {
            "sound": ['policy_details']
            }
        ],
    "functions": [
  
   { "fn": "SetBGTile('manu_common')",   "delay": 0},
  
        ],
    "name": "Manulife_4",
    "timing": -1,
    "index": 4
  },
 
   {
    "sprite_animations": [
     
     {"sprite": "white","x": 540,"y": 600,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1.5},
     
     {"sprite": "red_sub","x": 890,"y": 700,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1.5,"onClickFn":"goToPage(4)"},

     {"sprite": "left-arrow","x": 100,"y": 1800,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1,"onClickFn":"goToPage(4)"},
     {"sprite": "right-arrow","x": 980,"y": 1800,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1,"onClickFn":"goToPage(6)"},

 
        ],

    "text_animations": [

      {"text": [{"content":"Policy Details"}], "sx": 540,"sy": 200,"x": 540 ,"y": 200,"size": 50,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0.5,0],"align":"center","wordWrap":true,"wordWrapWidth":820 },
         {"text": [{"content":window.p_POLICY_NAME}], "sx": 540,"sy": 400,"x": 540 ,"y": 400,"size": 70,"weight":"bold","lineSpacing":-3,"color":"#000000","tween_type": "Elastic.easeOut","timing": 200,"delay":0,"align":"justify", "anchor":[0.5,0],"wordWrap":true,"wordWrapWidth":820,"strokeThickness":2},

   
      {"text": [{"content":"Beneficiary/ies:"}], "sx": 140,"sy": 700,"x": 140 ,"y": 700,"size": 46,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0,0],"wordWrap":true,"wordWrapWidth":820 },
      {"text": [{"content":window.p_benefi}], "sx": 140,"sy": 760,"x": 140 ,"y": 760,"size": 46,"weight":"bold","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0,0],"wordWrap":true,"wordWrapWidth":500,"strokeThickness":2,"align":"left"},

  
        ],

        "input_animations": [
     
        ],
    "sound_list": [
        {
            "sound": ['list_benefi']
            }
        ],
    "functions": [
  
   { "fn": "SetBGTile('manu_common')",   "delay": 0},
  
        ],
    "name": "Manulife_5",
    "timing": -1,
    "index": 5
  },
   {
    "sprite_animations": [
     
      {"sprite": "manu_ex","x": 540,"y": 450,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1},
      {"sprite": "manu_ex","x": 540,"y": 700,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1},
      {"sprite": "manu_ex","x": 540,"y": 950,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1},
      {"sprite": "manu_ex","x": 540,"y": 1200,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1},

     // {"sprite": "save","x": 540,"y": 1810,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":0.8,"onClickFn":"open_url_entroll()"},
 
    //  {"sprite": "bar_06","x": 670,"y": 1610,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":0.8,"onClickFn":"open_url_entroll()"},
     
       {"sprite": "left-arrow","x": 100,"y": 1800,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1,"onClickFn":"goToPage(4)"},
      {"sprite": "right-arrow","x": 980,"y": 1800,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1,"onClickFn":"q3_fill()"},


        ],

    "text_animations": [

      {"text": [{"content":"Payment Information"}], "sx": 540,"sy": 200,"x": 540 ,"y": 200,"size": 50,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0.5,0],"align":"center","wordWrap":true,"wordWrapWidth":820},

     {"text": [{"content":"Premium Amount:"}], "sx": 140,"sy": 500,"x": 140 ,"y": 500,"size": 46,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0,0],"wordWrap":true,"wordWrapWidth":820 },
      {"text": [{"content":"Payment Mode:"}], "sx": 140,"sy": 750,"x": 140 ,"y": 750,"size": 46,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0,0],"wordWrap":true,"wordWrapWidth":820 },
      {"text": [{"content":"Years to Pay:"}], "sx": 140,"sy": 1000,"x": 140 ,"y": 1000,"size": 46,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0,0],"wordWrap":true,"wordWrapWidth":820 },
      {"text": [{"content":"Enrolled in Autopay:"}], "sx": 140,"sy": 1250,"x": 140 ,"y": 1250,"size": 46,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0,0],"wordWrap":true,"wordWrapWidth":820 },

      // {"text": [{"content":"Not yet in Autopay?Tap "}], "sx": 370,"sy": 1600,"x": 370 ,"y": 1600,"size": 44,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0.5,0],"align":"center","wordWrap":true,"wordWrapWidth":820 },
      // {"text": [{"content":"Enroll"}], "sx": 668,"sy": 1600,"x": 668 ,"y": 1600,"size": 44,"weight":"bold","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0.5,0],"align":"center","strokeThickness":1 },
      // {"text": [{"content":" to sign up"}], "sx": 833,"sy": 1600,"x": 833 ,"y": 1600,"size": 44,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0.5,0],"align":"center","wordWrap":true,"wordWrapWidth":820 },
      
       {"text": [{"content":window.p_PREMIUM_AMOUNT+php}], "sx": 140,"sy": 570,"x": 140 ,"y": 570,"size": 46,"weight":"bold","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0,0],"strokeThickness":2},
        {"text": [{"content":window.p_PAYMENT_MODE}], "sx": 140,"sy": 820,"x": 140,"y": 820,"size": 46,"weight":"bold","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0,0],"strokeThickness":2},
        {"text": [{"content":window.p_pay_year}], "sx": 140,"sy": 1070,"x": 140 ,"y": 1070,"size": 46,"weight":"bold","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0,0],"strokeThickness":2},
       {"text": [{"content":window.p_entroll}], "sx": 140,"sy": 1320,"x": 140 ,"y": 1320,"size": 46,"weight":"bold","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0,0],"strokeThickness":2},

  
        ],

        "input_animations": [
      //   {"text": [{"content":""}],"placeHolder": " ","key": "in_otp","sx": 410,"sy": 1730,  "x": 420,"y": 1730,"size": 45,"weight": "bold","width": 240,"backgroundColor": "#7a7474","fill": "#000000","tween_type": "Elastic.easeOut","timing": 200,"delay": 0,"anchor": [0, 0],"height":70},
        //  {"text": [{"content": email}],"placeHolder": " ","key": "in_email","sx": 400,"sy": 1380,  "x": 450,"y": 1240,"size": 40,"weight": "bold","width": 450,"backgroundColor": "#d1e8f0","fill": "#013781","tween_type": "Elastic.easeOut","timing": 200,"delay": 0,"anchor": [0, 0]},
        ],
    "sound_list": [
        {
            "sound": ["payment_details","autopay"]
            }
        ],
    "functions": [
  
   { "fn": "SetBGTile('manu_common')",   "delay": 0},
     { "fn": "autopay_call()",   "delay": 0},

        ],
    "name": "Manulife_6",
    "timing": -1,
    "index": 6
  },
   {
    "sprite_animations": [
     
    {"sprite": "manu_ex","x": 540,"y": 450,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1},
    {"sprite": "manu_min","x": 540,"y": 720,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1},
    {"sprite": "manu_min","x": 540,"y": 960,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1},
    {"sprite": "manu_min","x": 540,"y": 1200,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1},


    {"sprite": "fb_blue","x": 315,"y": 840,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1,"onClickFn":"greentick('yes')"},
    {"sprite": "fb_blue2","x": 765,"y": 840,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1,"onClickFn":"greentick('no')"},

    {"sprite": "fb_blue","x": 315,"y": 1080,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1,"onClickFn":"greentick2('yes')"},
    {"sprite": "fb_blue2","x": 765,"y": 1080,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1,"onClickFn":"greentick2('no')"},

    {"sprite": "fb_blue","x": 315,"y": 1320,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1,"onClickFn":"greentick9('yes')"},
    {"sprite": "fb_blue2","x": 765,"y": 1320,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1,"onClickFn":"greentick9('no')"},


    {"sprite": "left-arrow","x": 100,"y": 1800,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1,"onClickFn":"goToPage(6)"},
    {"sprite": "right-arrow","x": 980,"y": 1800,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1,"onClickFn":"three_question()"},

        ],

    "text_animations": [

    {"text": [{"content":"Purchase Experience"}], "sx": 540,"sy": 200,"x": 540 ,"y": 200,"size": 50,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0.5,0],"align":"center","wordWrap":true,"wordWrapWidth":820},

   {"text": [{"content":"Your Agent:"}], "sx": 140,"sy": 500,"x": 140 ,"y": 500,"size": 46,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0,0],"wordWrap":true,"wordWrapWidth":820 },

   {"text": [{"content": window.p_insurance_agent}], "sx": 140,"sy": 570,"x": 140 ,"y": 570,"size": 46,"weight":"bold","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0,0],"strokeThickness":2 },

    {"text": [{"content":"Was your agent the one who discussed the application with you?"}], "sx": 540,"sy": 720,"x": 540 ,"y": 720,"size": 40,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0.5,0],"align":"center","wordWrap":true,"wordWrapWidth":850,"lineSpacing":-5 },

   {"text": [{"content":"Were you able to discuss the purpose and reason for purchasing your Manulife policy?"}], "sx": 540,"sy": 960,"x": 540 ,"y": 960,"size": 40,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0.5,0],"align":"center","wordWrap":true,"wordWrapWidth":850,"lineSpacing":-5 },

   {"text": [{"content":"Were you able to meet your agent in person, face to face?"}], "sx": 540,"sy": 1200,"x": 540 ,"y": 1200,"size": 40,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0.5,0],"align":"center","wordWrap":true,"wordWrapWidth":850,"lineSpacing":-5 },

   {"text": [{"content":"Please let us know if you have any other comments or feedback about your experience"}], "sx": 540,"sy": 1450,"x": 540 ,"y": 1450,"size": 40,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0.5,0],"align":"center","wordWrap":true,"wordWrapWidth":850,"lineSpacing":-5 },

        ],

        "input_animations": [
   
     {"text": [{"content":window.p_feedback}],"placeHolder": " ","key": "in_feedback","sx": 540,"sy": 1560,  "x": 540,"y": 1560,"size": 44,"weight": "bold","width": 830,"backgroundColor": "#c2ffae","fill": "#000000","tween_type": "Elastic.easeOut","timing": 200,"delay": 0,"anchor": [0.5, 0],"height":220,"type":'textarea'},
     
        ],
    "sound_list": [
        {
            "sound": ['purchase_experience']
            }
        ],
    "functions": [
  
   { "fn": "SetBGTile('manu_common')",   "delay": 0},
  
        ],
    "name": "Manulife_7",
    "timing": -1,
    "index": 7
  },

  {
    "sprite_animations": [

    {"sprite": "manu_min","x": 540,"y": 350,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1},
    {"sprite": "manu_min","x": 540,"y": 570,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1},
    {"sprite": "manu_min","x": 540,"y": 790,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1},
    {"sprite": "manu_min","x": 540,"y": 1010,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1},
    {"sprite": "manu_min","x": 540,"y": 1230,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1},
    {"sprite": "manu_min","x": 540,"y": 1450,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1},

    //q1 
      {"sprite": "fb_blue","x": 315,"y": 470,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1,"onClickFn":"greentick3('yes')"},
      {"sprite": "fb_blue2","x": 765,"y": 470,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1,"onClickFn":"greentick3('no')"},

     //q2
      {"sprite": "fb_blue","x": 315,"y": 690,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1,"onClickFn":"greentick4('yes')"},
      {"sprite": "fb_blue2","x": 765,"y": 690,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1,"onClickFn":"greentick4('no')"},

 //q3 
      {"sprite": "fb_blue","x": 315,"y": 910,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1,"onClickFn":"greentick5('yes')"},
      {"sprite": "fb_blue2","x": 765,"y": 910,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1,"onClickFn":"greentick5('no')"},

 //q4 
      {"sprite": "fb_blue","x": 315,"y": 1130,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1,"onClickFn":"greentick6('yes')"},
      {"sprite": "fb_blue2","x": 765,"y": 1130,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1,"onClickFn":"greentick6('no')"},

 //q5 
      {"sprite": "fb_blue","x": 315,"y": 1340,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1,"onClickFn":"greentick7('yes')"},
      {"sprite": "fb_blue2","x": 765,"y": 1340,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1,"onClickFn":"greentick7('no')"},

//q6 
      {"sprite": "fb_blue","x": 315,"y": 1570,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1,"onClickFn":"greentick8('yes')"},
      {"sprite": "fb_blue2","x": 765,"y": 1570,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1,"onClickFn":"greentick8('no')"},

 
     {"sprite": "left-arrow","x": 100,"y": 1800,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1,"onClickFn":"goToPage(7),sprite_three()"},
      {"sprite": "right-arrow","x": 980,"y": 1800,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1,"onClickFn":"six_question()"},

 
        ],

    "text_animations": [

   {"text": [{"content":"Non-Face to Face Experience"}], "sx": 540,"sy": 200,"x": 540 ,"y": 200,"size": 50,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0.5,0],"align":"center","wordWrap":true,"wordWrapWidth":820},

    {"text": [{"content":"Was the meeting done via video conferencing?"}], "sx": 540,"sy": 370,"x": 540 ,"y": 370,"size": 38,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0.5,0],"align":"center","wordWrap":true,"wordWrapWidth":900,"lineSpacing":-5 },

   {"text": [{"content":"Were features, benefits, and illusrations  fully explained during the meeting?"}], "sx": 540,"sy": 570,"x": 540 ,"y": 570,"size": 38,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0.5,0],"align":"center","wordWrap":true,"wordWrapWidth":900,"lineSpacing":-5 },

   {"text": [{"content":"Were you in the Philippines when the appliation was discussed and signed?"}], "sx": 540,"sy": 790,"x": 540 ,"y": 790,"size": 38,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0.5,0],"align":"center","wordWrap":true,"wordWrapWidth":900,"lineSpacing":-5 },

  {"text": [{"content":"Did you personally fill_up the application form, or authorized the agent to fill in your behalf?"}], "sx": 540,"sy": 1010,"x": 540 ,"y": 1010,"size": 38,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0.5,0],"align":"center","wordWrap":true,"wordWrapWidth":900,"lineSpacing":-5 },

   {"text": [{"content":"Did you submit the documents using your email address on record?"}], "sx": 540,"sy": 1230,"x": 540 ,"y": 1230,"size": 38,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0.5,0],"align":"center","wordWrap":true,"wordWrapWidth":900,"lineSpacing":-5 },

   {"text": [{"content":"Do you confirm the representations cited in the attestation that you submitted?"}], "sx": 540,"sy": 1450,"x": 540 ,"y": 1450,"size": 38,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0.5,0],"align":"center","wordWrap":true,"wordWrapWidth":900,"lineSpacing":-5 },

  
        ],

        "input_animations": [
   //   {"text": [{"content":""}],"placeHolder": " ","key": "in_feedback","sx": 100,"sy": 1400,  "x": 100,"y": 1400,"size": 45,"weight": "bold","width": 790,"backgroundColor": "#c6bebe","fill": "#000000","tween_type": "Elastic.easeOut","timing": 200,"delay": 0,"anchor": [0, 0],"height":300},
        //  {"text": [{"content": email}],"placeHolder": " ","key": "in_email","sx": 400,"sy": 1380,  "x": 450,"y": 1240,"size": 40,"weight": "bold","width": 450,"backgroundColor": "#d1e8f0","fill": "#013781","tween_type": "Elastic.easeOut","timing": 200,"delay": 0,"anchor": [0, 0]},
        ],
    "sound_list": [
        {
            "sound": ['non_face']
            }
        ],
    "functions": [
  
   { "fn": "SetBGTile('manu_common')",   "delay": 0},
  
        ],
    "name": "Manulife_8",
    "timing": -1,
    "index": 8
  },

  {
    "sprite_animations": [
     
    
  {"sprite": "manu_black","x": 540,"y": 520,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1,"onClickFn":"open_url55()"},
  //   {"sprite": "manu_black","x": 540,"y": 820,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1,"onClickFn":"open_url56()"},

  {"sprite": "manu_black","x": 540,"y": 820,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1,"onClickFn":"agent_vcf()"},
  {"sprite": "manu_black","x": 540,"y": 1120,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1,"onClickFn":"customer_vcf()"},

  {"sprite": "left-arrow","x": 100,"y": 1800,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1,"onClickFn":"backquestion36()"},
  {"sprite": "right-arrow","x": 980,"y": 1800,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1,"onClickFn":"goToPage(10)"},


        ],

    "text_animations": [

  {"text": [{"content":"Downloads"}], "sx": 540,"sy": 200,"x": 540 ,"y": 200,"size": 50,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0.5,0],"align":"center","wordWrap":true,"wordWrapWidth":820},

  {"text": [{"content":"Click here to download your Product Brochure"}], "sx": 540,"sy": 450,"x": 540 ,"y": 450,"size": 43,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0.5,0],"align":"center","wordWrap":true,"wordWrapWidth":900,"lineSpacing":-5 },

 // {"text": [{"content":"Click here to download your Official Receipt"}], "sx": 540,"sy": 750,"x": 540 ,"y": 750,"size": 43,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0.5,0],"align":"center","wordWrap":true,"wordWrapWidth":900,"lineSpacing":-5 },
  {"text": [{"content":"Click here to download Agent Contact Card"}], "sx": 540,"sy": 750,"x": 540 ,"y": 750,"size": 43,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0.5,0],"align":"center","wordWrap":true,"wordWrapWidth":900,"lineSpacing":-5 },

  {"text": [{"content":"Click here to download Customer Care Contact Card"}], "sx": 540,"sy": 1015,"x": 540 ,"y": 1015,"size": 43,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0.5,0],"align":"center","wordWrap":true,"wordWrapWidth":900,"lineSpacing":-5 },



  {"text": [{"content":"PRODUCT BROCHURE"}], "sx": 540,"sy": 543,"x": 540 ,"y": 543,"size": 42,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0.5,0],"align":"center","wordWrap":true,"wordWrapWidth":750,"strokeThickness":1},
  //{"text": [{"content":"OFFICIAL RECEIPT"}], "sx": 540,"sy": 843,"x": 540 ,"y": 843,"size": 42,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0.5,0],"align":"center","wordWrap":true,"wordWrapWidth":750,"strokeThickness":1},


  {"text": [{"content":"AGENT CONTACT CARD"}], "sx": 540,"sy": 843,"x": 540 ,"y": 843,"size": 42,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0.5,0],"align":"center","wordWrap":true,"wordWrapWidth":750,"strokeThickness":1},
  {"text": [{"content":"CUSTOMER CARE CONTACT CARD"}], "sx": 540,"sy": 1143,"x": 540 ,"y": 1143,"size": 42,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0.5,0],"align":"center","wordWrap":true,"wordWrapWidth":750,"strokeThickness":1},

        ],

        "input_animations": [
        
         // {"text": [{"content":""}],"placeHolder": " ","key": "in_datetime","sx": 100,"sy": 1410,  "x": 100,"y": 1410,"size": 45,"weight": "bold","width": 790,"backgroundColor": "#c6bebe","fill": "#000000","tween_type": "Elastic.easeOut","timing": 200,"delay": 0,"anchor": [0, 0],"height":280},

        ],
    "sound_list": [
        {
            "sound": ['new_download']
            }
        ],
    "functions": [
  
   { "fn": "SetBGTile('manu_common')",   "delay": 0},
      { "fn": "hideout()",   "delay": 0},

  
        ],
    "name": "Manulife_9",
    "timing": -1,
    "index": 9
  },
  {
    "sprite_animations": [
     
      
      {"sprite": "save","x": 315,"y": 850,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1.2,"onClickFn":"agree_api()"},
      {"sprite": "save","x": 765,"y": 850,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1.2,"onClickFn":"showOTP2()"},
    //     {"sprite": "hashcolor","x": 540,"y": 1400,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1.8,"onClickFn":"datetime_api()"},

     {"sprite": "left-arrow","x": 100,"y": 1800,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1,"onClickFn":"goToPage(9)"},
      {"sprite": "right-arrow","x": 980,"y": 1800,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1,"onClickFn":"tq_go()"},

 
        ],

    "text_animations": [

       {"text": [{"content":"Do you agree with all your policy details?"}], "sx": 540,"sy": 650,"x": 540 ,"y": 650,"size": 60,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0.5,0],"align":"center","wordWrap":true,"wordWrapWidth":900,"lineSpacing":-5 },

        {"text": [{"content":"Agree"}], "sx": 315,"sy": 910,"x": 315 ,"y": 910,"size": 50,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0.5,0.5],"align":"center" },
        {"text": [{"content":"Contact Me"}], "sx": 765,"sy": 910,"x": 765 ,"y": 910,"size": 50,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0.5,0.5],"align":"center" },

       //  {"text": [{"content":"Date"}], "sx": 315,"sy": 1070,"x": 315 ,"y": 1070,"size": 50,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0.5,0.5],"align":"center" },
       //  {"text": [{"content":"Time"}], "sx": 765,"sy": 1070,"x": 765 ,"y": 1070,"size": 50,"weight":"normal","color":"#000000","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0.5,0.5],"align":"center" },
       // {"text": [{"content":"Submit"}], "sx": 540,"sy": 1460,"x": 540 ,"y": 1460,"size": 50,"weight":"bold","color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 500,"delay":0, "anchor":[0.5,0.5],"align":"center" },

  
        ],

        "input_animations": [
        
         // {"text": [{"content":""}],"placeHolder": " ","key": "in_datetime","sx": 100,"sy": 1410,  "x": 100,"y": 1410,"size": 45,"weight": "bold","width": 790,"backgroundColor": "#c6bebe","fill": "#000000","tween_type": "Elastic.easeOut","timing": 200,"delay": 0,"anchor": [0, 0],"height":280},

        ],
    "sound_list": [
        {
            "sound": ["new_agree","datetime"]
            }
        ],
    "functions": [
  
   { "fn": "SetBGTile('manu_agree')",   "delay": 0},
  //  { "fn": "showOTP2()",   "delay": 1},
       
    
        ],
    "name": "Manulife_10",
    "timing": -1,
    "index": 10
  },
 
   {
    "sprite_animations": [
     

       {"sprite": "bg_bar","x": 520,"y": 1350,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":0.6,"onClickFn":"open_url_manu()"},
     
       {"sprite": "box_blank","x": 390,"y": 1750,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1.5,"onClickFn":"link_fb()"},
     
       {"sprite": "box_blank","x": 540,"y": 1750,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1.5,"onClickFn":"link_twitter()"},
     
       {"sprite": "box_blank","x": 660,"y": 1750,"loop": false,"timing": 0,"delay": 0, "toTopObj":1, "anchor":[0.5,0],"scale":1.5,"onClickFn":"link_instagram()"},
     

          ],

    "text_animations": [

  //{"text": [{"content":"Share your association with Manulife across your social media"}], "sx": 700,"sy": 1410,"x": 700 ,"y": 1410,"size": 40,"weight":"bold","lineSpacing":-3,"color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0,"align":"center", "anchor":[0.5,0],"wordWrap":true,"wordWrapWidth":700,"lineSpacing":-3 },

  //  {"text": [{"content":"I have ensured my loved ones are protected even during my absence if you really care for your loved one insure yourself today and show your love and protection."}], "sx": 420,"sy": 1600,"x": 410 ,"y": 1100,"size": 28,"weight":"normal","lineSpacing":-6,"color":"#FFFFFF","tween_type": "Elastic.easeOut","timing": 200,"delay":0,"align":"center", "anchor":[0.5,0],"wordWrap":true,"wordWrapWidth":400,"lineSpacing":-3 },

  
        ],

        "input_animations": [
      //   {"text": [{"content":""}],"placeHolder": " ","key": "in_otp","sx": 410,"sy": 1730,  "x": 420,"y": 1730,"size": 45,"weight": "bold","width": 240,"backgroundColor": "#7a7474","fill": "#000000","tween_type": "Elastic.easeOut","timing": 200,"delay": 0,"anchor": [0, 0],"height":70},
        //  {"text": [{"content": email}],"placeHolder": " ","key": "in_email","sx": 400,"sy": 1380,  "x": 450,"y": 1240,"size": 40,"weight": "bold","width": 450,"backgroundColor": "#d1e8f0","fill": "#013781","tween_type": "Elastic.easeOut","timing": 200,"delay": 0,"anchor": [0, 0]},
        ],
    "sound_list": [
        {
            "sound": ['new_thankyou']
            }
        ],
    "functions": [
  
   { "fn": "SetBGTile('tq_manu')",   "delay": 0},
    { "fn": "hideout()",   "delay": 0},
  
        ],
    "name": "Manulife_11",
    "timing": -1,
    "index": 11
  },
 
 

];



for(var i=0; i<screens_eng.length; i++ )
{
    if(i==5)
    {
        if(window.pa_PREMIUM_POLICY_TYPE!==false)
        {
            if(window.pa_PREMIUM_POLICY_TYPE==='regular')
            {
                console.log("Enter : regular");
                screens_eng[i]['sound_list'] = [{"sound": ["au_5_1", "$var.window.pa_product", "au_5_2", "$var.window.pa_category", "au_5_3", "$var.window.pa_PREMIUM_POLICY_TYPE", "au_5_4", "$var.currency_window.p_PREMIUM_AMOUNT", "$var.window.pa_FREQUENCY", "au_5_5", "$var.number_window.pa_PAYMENT_TERM", "years", "au_5_6", "$var.number_window.pa_BENEFIT_TERM", "au_5_7", "$var.currency_window.p_SUM_ASSURED", "au_5_8"]}];
            }
            else if(window.pa_PREMIUM_POLICY_TYPE==='single')
            {
                console.log("Enter : single");
                screens_eng[i]['sound_list'] = [{"sound": ["au_5_1", "$var.window.pa_product", "au_5_2", "$var.window.pa_category", "au_5_3", "$var.window.pa_PREMIUM_POLICY_TYPE", "au_5_4_1", "$var.currency_window.p_PREMIUM_AMOUNT", "au_5_6", "$var.number_window.pa_BENEFIT_TERM", "au_5_7", "$var.currency_window.p_SUM_ASSURED", "au_5_8"]}];
            }
        }
    }
    window.stage.screens.push(screens_eng[i]);
}



