var security_fpath='./assets/js/common/plugins/security/';


$(document).ready(function(){
    $("#loading_div").show();
	
	window.cur_url = window.location.href.trim();  
	
	$.getScript(security_fpath+'Encryption.js',  function(){
		$.getScript(security_fpath+'crypto-js.js',  function(){
			$.getScript(security_fpath+'secure_api_helper.js',  function(){
				
				window.kfd_api_url= 'https://dev.anoorcloud.in/manu-en/portal/';
				var kfd_validate_url_api= window.kfd_api_url+'api/validate-welcome-call';
				
				window.geo_latitude = 0;
				window.geo_longitude = 0;
				window.geo_location = '';

				window.app_view = false;
				
				var send_data='{\"manu_wc_url\":\"'+window.cur_url+'\"}';
								
				var data = callEncryptedAPI(kfd_validate_url_api,send_data).then(function(data)
				{
					console.log('DATA ',data);
				
					if(data.status)
					{
						console.log("enter : main");
						if(data.expired)
						{
							window.product = "expiry";
							window.product_slug = GetFriendlyName(window.product);
							window.flow_slug = GetFriendlyName(window.product);
						   load_game_js(window.flow_slug);
						}
						else if(data.completed)
						{
							window.product = "completed";
							window.product_slug = "completed";
							window.flow_slug = "completed";
						   load_game_js(window.flow_slug);
						}
						else if((data.completed == false) && (data.company_code))
						{   
								
							if(data.company_code == "MANULIFE"){
								// alert("completed MANULIFE")
								window.product = "MANULIFE";
								window.product_slug = "manulife";
								window.flow_slug = "manulife";
							   load_game_js(window.flow_slug);
						   }
							else {

								window.product = "MCBL";
								window.product_slug = "mcbl";
								window.flow_slug = "mcbl";
								load_game_js(window.flow_slug);
						   }
							window.policy_no=data.policy_no;
							   
							// alert(window.policy_no);

						}
					 }
					
				});				
		
			});
		});
	});
	   

    function load_game_js(slug)
    {
        $.getScript("./assets/product_assets/" + slug + "/js/flow_assets_init.js",  function(){
            // Load card workflow js
            $.getScript( "./assets/product_assets/" + slug + "/js/flow_init.js",  function(){
                $("#loading_div").hide();
                $.getScript( "./assets/js/common/anoor/game.js", function(){ } );
                
            });
        });
    }
	
	
});

