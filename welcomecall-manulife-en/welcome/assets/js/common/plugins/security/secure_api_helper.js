
var token = 'An00rCl0ud';
var encryptionObj=null;

function encryptData(data)
{
	if(encryptionObj==null)
		encryptionObj = new Encryption();
	
	return encryptionObj.encrypt(data, token);
}

function decryptData(data)
{
	if(encryptionObj==null)
		encryptionObj = new Encryption();
	
	return encryptionObj.decrypt(data.replace(/['"]+/g, ''), token);
}

async function callEncryptedAPI(api,data)
{
	var result = await api_call(api,data);
	
	console.log('Returning Result ',result);
	
	return result;
}

function api_call(api,data)
{
	return new Promise(function (resolve, reject) { 
	
		var form = new FormData();
		form.append("data",encryptData(data));
		
		var settings = {
		  "async": true,
		  "crossDomain": true,
		  "url": api,
		  "method": "POST",
		  "headers": {
			"cache-control": "no-cache",
			"postman-token": "a6490f6b-13ac-3b94-1f10-e175a93d299a"
		  },
		  "processData": false,
		  "contentType": false,
		  "mimeType": "multipart/form-data",
		  "data": form
		}
		
		$.ajax(settings).done(function (response) {
			
			if(response!=null)
			{
				var response_data = JSON.parse(decryptData(response));
				
				console.log('API Response = ',response_data);
				
				resolve(response_data);
			}
			else
			{
				reject(null);
			}
		});
		   
    }); 
}
