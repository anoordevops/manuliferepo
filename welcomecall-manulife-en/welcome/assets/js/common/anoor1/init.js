

$(document).ready(function(){
    $("#loading_div").show();
    window.cur_url = window.location.href.trim();	


    window.geo_latitude = 0;
    window.geo_longitude = 0;
    window.geo_location = '';

    window.app_view = false;
   

    function load_game_js(slug)
    {
        $.getScript("./assets/product_assets/" + slug + "/js/flow_assets_init.js",  function(){
            // Load card workflow js
            $.getScript( "./assets/product_assets/" + slug + "/js/flow_init.js",  function(){
                $("#loading_div").hide();
                $.getScript( "./assets/js/common/anoor/game.js", function(){ } );
                
            });
        });
    }

  window.product ="active_assure_diamond";
  window.product_slug = "active_assure_diamond";
  window.flow_slug = "active_assure_diamond";
  load_game_js("active_assure_diamond");

});
