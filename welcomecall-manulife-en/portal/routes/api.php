<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post("/create-welcome-call", 'ApiController@createWelcomeCallLink');

Route::post("/validate-welcome-call", 'ApiController@validateWelcomeCall');

Route::post("/generate-otp", 'ApiController@generateUserOTP');

Route::post("/validate-otp", 'ApiController@validateOTP');

Route::post("/update-policy-details", 'ApiController@updatePolicyDetails');

Route::post("/update-completed-status", 'ApiController@completedStatus');

Route::get("/agent-vcf/{id}", 'ApiController@agentVCF');

Route::get("/customer-care-vcf", 'ApiController@customerCareVCF');

Route::get("/test", 'ApiController@test');
