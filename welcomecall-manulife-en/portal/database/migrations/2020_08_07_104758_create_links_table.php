<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('links', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('company_code')->nullable();
            $table->string('policy_no');
            $table->string('policy_name')->nullable();
            $table->string('link')->nullable();
            $table->string('short_link')->nullable();
            $table->string('mobile_no')->nullable();
            $table->integer('otp')->nullable();
            $table->string('name')->nullable();
            $table->string('address')->nullable();
            $table->string('phone_no')->nullable();
            $table->string('email')->nullable();
            $table->string('insurance_coverage')->nullable();
            $table->string('intial_investment')->nullable();
            $table->json('beneficiaries_list')->nullable();
            $table->string('premium_amount')->nullable();
            $table->string('payment_mode')->nullable();
            $table->string('payment_years')->nullable();
            $table->boolean('autopay')->nullable();
            $table->string('agent_name')->nullable();
            $table->string('agent_number')->nullable();
            $table->string('agent_email')->nullable();
            $table->string('cc_number1')->nullable();
            $table->string('cc_number2')->nullable();
            $table->string('cc_email')->nullable();
            $table->string('product_code')->nullable();
            $table->boolean('purchase_experience_1')->nullable();
            $table->boolean('purchase_experience_2')->nullable();
            $table->boolean('purchase_experience_3')->nullable();
            $table->text('purchase_experience_feedback')->nullable();
            $table->boolean('q1')->nullable();
            $table->boolean('q2')->nullable();
            $table->boolean('q3')->nullable();
            $table->boolean('q4')->nullable();
            $table->boolean('q5')->nullable();
            $table->boolean('q6')->nullable();
            $table->boolean('acknowledgment')->nullable();
            $table->boolean('contact_me')->nullable();
            $table->dateTime('contact_me_time')->nullable();
            $table->dateTime('email_reminder_1')->nullable();
            $table->dateTime('email_reminder_2')->nullable();
            $table->dateTime('email_reminder_3')->nullable();
            $table->json('network_details')->nullable();
            $table->json('device_details')->nullable();
            $table->boolean('status')->default(1);
            $table->boolean('completed_status')->default(0);
            $table->dateTime('completed_on')->nullable();
            $table->boolean('is_open')->default(0);
            $table->dateTime('is_open_at')->nullable();
            $table->json('edited');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('links');
    }
}
