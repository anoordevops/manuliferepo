<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Links extends Model
{
  protected $table='links';

  protected $fillable = [ 'device_details', 'network_details', 'beneficiaries_list', 'edited'];

  protected $casts = [
    'device_details' => 'array',
    'network_details' => 'array',
    'beneficiaries_list' => 'array',
    'edited' => 'array'
  ];

  protected $attributes = [
      'edited' => '{
          "name": "",
          "address": "",
          "email": "",
          "phone": ""
      }'
  ];

  public function brochure()
  {
    return $this->hasOne('App\Brochure', 'policy_name', 'policy_name');
  }
}
