<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientPayload extends Model
{
  protected $table='client_payload';

  protected $casts = [
    'payload' => 'array'
  ];
}
