<?php

namespace App\Http\Controllers;

use App\ClientPayload;
use App\Links;
use App\Logs;
use App\Brochure;
use App\Encryption;
use Response;
use JeroenDesloovere\VCard\VCard;

use Jenssegers\Agent\Agent;
use Illuminate\Http\Request;

class ApiController extends Controller
{
  public function logs($proposal,$mod,$req,$res)
  {
    $log = new Logs;
    $log->proposal_no = $proposal;
    $log->module = $mod;
    $log->request = $req;
    $log->response = $res;
    $log->save();
    return;
  }

  public function createWelcomeCallLink(Request $request)
  {
    $details=$request->all();
    $policy=$details['policyNumber'];

    $count=Links::where('policy_no',$policy)->count();

    if($count==1)
    {
      $link=Links::where('policy_no', $policy)->get();
      $url=$link[0]->short_link;
      $array=array('status'=>true,'url'=>$url,'message'=>'Policy no already exist');
      $result=json_encode($array);
      ApiController::logs($policy,'recreate',json_encode($details),$result);
      return Response()->json($array);
    }

    $link=env("APP_LINK_URL", "https://dev.anoorcloud.in/manu/welcome/index.html?").base64_encode($policy);

    $address = "";

    if(isset($details['address'])) {
      $add = json_decode($details['address'],true);

      $address = isset($add[0]['address1']) ? $add[0]['address1'].', ' : '';
      $address .= isset($add[0]['address2']) ? $add[0]['address2'].', ' : '';
      $address .= isset($add[0]['address3']) ? $add[0]['address3'].', ' : '';
      $address .= isset($add[0]['address4']) ? $add[0]['address4'].', ' : '';
      $address .= isset($add[0]['zip']) ? $add[0]['zip'] : '';
    }
    $url=ApiController::linkgen($link);
    $links = new Links;
    $links->company_code = isset($details['companyCode']) ? $details['companyCode']: '';
    $links->policy_no = $policy;
    $links->policy_name = isset($details['policyName']) ? $details['policyName']: '';
    $links->name = isset($details['firstName']) ? $details['firstName']: '';
    $links->mobile_no = isset($details['mobilePhone']) ? $details['mobilePhone']: '';
    $links->address = $address;
    $links->phone_no = isset($details['homePhone']) ? $details['homePhone']: '';
    $links->email = isset($details['email']) ? $details['email']: '';
    $links->insurance_coverage = isset($details['insuranceCoverage']) ? $details['insuranceCoverage']: '';
    $links->intial_investment = isset($details['initialInvestment']) ? $details['initialInvestment']: '';
    $links->beneficiaries_list = isset($details['beneficiaries']) ? $details['beneficiaries']: '';
    $links->premium_amount = isset($details['premiumAmount']) ? $details['premiumAmount']: '';
    $links->payment_mode = isset($details['paymentMode']) ? $details['paymentMode']: '';
    $links->payment_years = isset($details['yearsPayment']) ? $details['yearsPayment']: '';
    $links->autopay = isset($details['enrolledAutopay']) ? 1 : 0;
    $links->agent_name = isset($details['agentName']) ? $details['agentName']: '';
    $links->agent_number = isset($details['agentContactNo']) ? $details['agentContactNo']: '';
    $links->agent_email = isset($details['agentEmail']) ? $details['agentEmail']: '';
    $links->link = $link;
    $links->short_link = $url;
    $links->save();

    $array=array('status'=>true, 'url'=>$url);
    $result=json_encode($array);
    ApiController::logs($policy, 'create', json_encode($details), $result);

    $payload = new ClientPayload;
    $payload->policy_no = $policy;
    $payload->payload = $details;
    $payload->save();

    return Response()->json($array);
  }

  public function validateWelcomeCall(Request $request)
  {
    $Encryption = new Encryption();
    $endata = $request->data;
    $endata = $Encryption->decrypt($endata, env("ENCRYPT_KEY", ""));
    $endata = json_decode($endata);

    if(isset($endata->manu_wc_url)){
      $url=$endata->manu_wc_url;
      $policy_no = isset(explode('?',$url)[1]) ? base64_decode(explode('?',$url)[1]) : "";
      $arr=[];
      $links=Links::where('policy_no', $policy_no)->first();
      if($links) {
        $status=$links->completed_status;
        if($status == 1) {
          $arr['status']=true;
          $arr['expired']=false;
          $arr['completed']=true;
        } else {
          $arr['status']=true;
          $arr['expired']=false;
          $arr['completed']=false;
          $arr['policy_no']=$links->policy_no;
          $arr['company_code']=$links->company_code;
        }
      } else {
        $policy_no = "Invalid";
        $arr['status']=false;
        $arr['message']="Invalid Policy Number";
      }
    } else {
      $policy_no = "Invalid";
      $arr['status']=false;
      $arr['message']="Invalid data";
    }
    ApiController::logs($policy_no,'vaildate_wc',json_encode($endata), json_encode($arr));
    $encrypted = $Encryption->encrypt(json_encode($arr), env("ENCRYPT_KEY", ""));

    return Response()->json($encrypted);
  }

  public function generateUserOTP(Request $request)
  {
    $Encryption = new Encryption();
    $endata = $request->data;
    $endata = $Encryption->decrypt($endata, env("ENCRYPT_KEY", ""));
    $endata = json_decode($endata);

    $policy_no = isset($endata->policy_no) ? $endata->policy_no : "";
    $links = Links::where('policy_no', $policy_no)->first();
    $arr = [];
    if($links) {
      $otp = ApiController::generateOTP();
      $links->otp = $otp;
      $links->save();
      $arr['status'] = true;
      $arr['otp'] = $otp;
    } else {
      $policy_no = "Invalid";
      $arr['status'] = false;
    }
    ApiController::logs($policy_no,'generate_otp',json_encode($endata), json_encode($arr));
    $encrypted = $Encryption->encrypt(json_encode($arr), env("ENCRYPT_KEY", ""));

    return Response()->json($encrypted);
  }

  public function validateOTP(Request $request)
  {
    $Encryption = new Encryption();
    $endata = $request->data;
    $endata = $Encryption->decrypt($endata, env("ENCRYPT_KEY", ""));
    $endata = json_decode($endata);

    $policy_no = isset($endata->policy_no) ? $endata->policy_no : "";
    $otp = isset($endata->otp) ? $endata->otp: "";
    $arr = [];
    $links = Links::where('policy_no', $policy_no)->where('otp', $otp)->with('brochure')->first();
    if($links) {
      $status=$links->status;
      $agent=ApiController::agent();
      $links->device_details = $agent;
      if ($links->is_open == 0) {
        $links->is_open = 1;
        $links->is_open_at = date('Y-m-d H:i:s');
      }
      $links->save();
      if(!empty($links))
      {
        $params=array();
        $params['policy_no'] = $policy_no;
        $params['name'] = $links->name;
        $params['address']=$links->address;
        $params['mobile_no']=$links->mobile_no;
        $params['email']=$links->email;
        $params['insurance_coverage']=$links->insurance_coverage;
        $params['intial_investment']=$links->intial_investment;
        $params['beneficiaries_list']=$links->beneficiaries_list;
        $params['premium_amount']=$links->premium_amount;
        $params['payment_mode']=$links->payment_mode;
        $params['payment_years']=$links->payment_years;
        $params['autopay']=$links->autopay;
        $params['agent_name']=$links->agent_name;
        if(!is_null($links->brochure)){
          $params['policy_name']=$links->brochure->generic_policy_name;
          $params['brochure_url']= $links->brochure->url;
        }

        $arr=array();
        $arr['status']=true;
        $arr['msg']='Given WELCOME CALL URL is valid!';
        $arr['output']=$params;
      } elseif ($status==0) {
        $arr=array();
        $arr['status']=false;
        $arr['msg']='Given WELCOME CALL URL is Invalid!';
      } elseif ($status==1 and $links->completed_status==1) {
        $arr=array();
        $arr['status']=true;
        $arr['expired']=false;
        $arr['completed']=true;
        $arr['msg']='Given WELCOME CALL already Completed';
      }
    } else {
      $policy_no = "Invalid";
      $arr['status'] = false;
    }
    ApiController::logs($policy_no,'validate_otp',json_encode($endata), json_encode($arr));
    $encrypted = $Encryption->encrypt(json_encode($arr), env("ENCRYPT_KEY", ""));

    return Response()->json($encrypted);
  }

  public function updatePolicyDetails(Request $request)
  {
    $policy_no = $request->policy_no;
    $type = $request->type;
    $arr = [];
    $links = Links::where('policy_no', $policy_no)->first();
    if($links) {
      if($type == "contact-details") {
        $edits = ["name" => 0, "address" => 0, "phone" => 0, "email" => 0];
        if($request->edited_name) {
          $edits["name"] = 1;
          $links->name = $request->name;
        }
        if($request->edited_address) {
          $edits["address"] = 1;
          $links->address = $request->address;
        }
        if($request->edited_phone) {
          $edits["phone"] = 1;
          $links->mobile_no = $request->mobile_no;
        }
        if($request->edited_email) {
          $edits["email"] = 1;
          $links->email = $request->email;
        }
        $links->edited = $edits;
      } elseif ($type == "question-set-1") {
        $links->purchase_experience_1 = ($request->experience1 == 'Yes') ? 1 : 0;
        $links->purchase_experience_2 = ($request->experience2 == 'Yes') ? 1 : 0;
        $links->purchase_experience_3 = ($request->experience3 == 'Yes') ? 1 : 0;
        $links->purchase_experience_feedback = $request->experience_feedback;
      } elseif ($type == "question-set-2") {
        $links->q1 = ($request->q1 == 'Yes') ? 1 : 0;
        $links->q2 = ($request->q2 == 'Yes') ? 1 : 0;
        $links->q3 = ($request->q3 == 'Yes') ? 1 : 0;
        $links->q4 = ($request->q4 == 'Yes') ? 1 : 0;
        $links->q5 = ($request->q5 == 'Yes') ? 1 : 0;
        $links->q6 = ($request->q6 == 'Yes') ? 1 : 0;
      } elseif ($type == "agree") {
        $links->acknowledgment = $request->acknowledgment;
        $links->contact_me = 0;
      } elseif ($type == "contact-me") {
        $links->acknowledgment = 0;
        $links->contact_me = $request->contact_me;
        $links->contact_me_time = date("Y-m-d", strtotime($request->datetime))." ".date("H:i:s", strtotime($request->datetime));
      }
      $links->save();
      $arr['status'] = true;
    } else {
      $arr['status'] = false;
    }

    return Response()->json($arr);
  }

  public function completedStatus(Request $request)
  {
    $Encryption = new Encryption();
    $endata = $request->data;
    $endata = $Encryption->decrypt($endata, env("ENCRYPT_KEY", ""));
    $endata = json_decode($endata);

    $policy_no = isset($endata->policy_no) ? $endata->policy_no : "";
    $status = isset($endata->completed_status) ? $endata->completed_status : "";
    $arr = [];
    $links = Links::where('policy_no', $policy_no)->first();
    if($links) {
      $links->completed_status = $status;
      $links->completed_on = date('Y-m-d H:i:s');
      $links->save();
      $arr['status'] = true;
    } else {
      $policy_no = "Invalid";
      $arr['status'] = false;
    }
    ApiController::logs($policy_no,'update_policy_details',json_encode($endata), json_encode($arr));
    $encrypted = $Encryption->encrypt(json_encode($arr), env("ENCRYPT_KEY", ""));

    return Response()->json($encrypted);
  }

  public function linkgen($url)
  {
    $data = array('api_key' => '6a748b-84f8bc-461d9a-dc658e-cc365e','url' => $url);
    $ch = curl_init('https://anoor.link/portal/api/shorten');
    curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLINFO_HEADER_OUT, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    $result = curl_exec($ch);
    $obj=json_decode($result);
    return $obj->url;
  }

  public function agent()
  {
    $agent = new Agent();
    $array=array();
    $array['languages']=$agent->languages();
    if($agent->isDesktop())
    {
      $device="Desktop";
    }
    elseif($agent->isMobile()) {
      $device="Mobile";
    }
    elseif($agent->isTablet()) {
      $device="Tablet";
    }
    elseif($agent->isPhone()) {
      $device="Phone";
    }
    else {
      $device=$agent->device();
    }
    $array['device']=$device;
    $array['os']=$agent->platform();
    $array['os_version']=$agent->version($agent->platform());
    $array['browser']=$agent->browser();
    $array['browser_version']=$agent->version($agent->browser());
    return json_encode($array);
  }

  public function generateOTP($length = '4') {
    $characters = '0123456789';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[rand(0, $charactersLength - 1)];
    }

    return $randomString;
  }

  public function agentVCF($policy_no)
  {
    $links = Links::where('policy_no', $policy_no)->first();
    if($links) {
      $vcard = new VCard();

      $lastname = '';
      $firstname = $links->agent_name;

      $vcard->addName($lastname, $firstname);

      $vcard->addCompany('Manulife');
      $vcard->addEmail($links->agent_email);
      $vcard->addPhoneNumber($links->agent_number, 'WORK');
      $vcard->addPhoto('https://dev.anoorcloud.in/manu/welcome/assets/images/common/Logo.png');
      return Response::make(
        $vcard->download(),
        200,
        $vcard->getHeaders(true)
      );
    }
  }

  public function customerCareVCF(Request $request)
  {
    $vcard = new VCard();

    $lastname = 'Customer Care';
    $firstname = 'Manulife';

    $vcard->addName($lastname, $firstname);

    $vcard->addCompany('Manulife');
    $vcard->addEmail('phcustomercare@manulife.com');
    $vcard->addPhoneNumber('+63288847000', 'WORK');
    $vcard->addPhoneNumber('1800-1888-6268', 'WORK');
    $vcard->addPhoto('https://dev.anoorcloud.in/manu/welcome/assets/images/common/Logo.png');
    return Response::make(
        $vcard->download(),
        200,
        $vcard->getHeaders(true)
    );
  }
}
