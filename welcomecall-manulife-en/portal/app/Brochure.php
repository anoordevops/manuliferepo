<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brochure extends Model
{
  protected $table='brochure';

  public function policyName()
  {
    return $this->belongsTo('App\Links', 'policy_name', 'policy_name');
  }
}
